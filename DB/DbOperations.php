<?php
require_once __DIR__ . '/db_config.php';

class DbOperations
{
    private $db;

    public function __construct(
        string $dsn,
        string $user,
        string $password
    )
    {
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];

        $this->db = new PDO($dsn, $user, $password, $options);
    }

    public function quote(string $string)
    {
        return $this->db->quote($string);
    }

    public function query($sql)
    {
        return $this->db->query($sql);
    }

    public function fetchAll(string $sql)
    {
        $pdoStatementObject = $this->query($sql);
        return $pdoStatementObject->fetchAll();
    }

    public function exec(string $sql)
    {
        return $this->db->exec($sql);
    }

}