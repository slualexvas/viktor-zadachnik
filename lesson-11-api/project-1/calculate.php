<?php
$apiRaw = file_get_contents('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');

// Если associative установлен как 1(true) то вернется массив, если associative установлен как 0(false) то вернется объект класса.
$apiDecodedResult = json_decode($apiRaw, 1);

//$course = 28 -- прошлое значение;

foreach ($apiDecodedResult as $item) {
    if ($item['ccy'] == 'USD' && $item['base_ccy'] == 'UAH') {
        // если $course = $apiDecodedResult[$item]['sale'], то возвращает INF(по англ. бесконечность)

        $course = $item['sale'];
        //        или foreach ($apiDecodedResult as $key => $item) и $course = $apiDecodedResult[Skey]['sale']
    }
}

// Второй вариант:

//$apiReindexedResult = [];
//foreach ($apiDecodedResult as $item) {
//    $currencyFrom = $item['ccy'];
//
//    if ($item['base_ccy'] != 'UAH') {
//        continue; // рассматриваем только курсы "валюта => гривны"
//    }
//
//    $apiReindexedResult[$currencyFrom] = $item['sale'];
//}
//$course = $apiReindexedResult['USD'];


echo "{$_POST['uah']} гривен -- это " . $_POST['uah'] / $course . " долларов";