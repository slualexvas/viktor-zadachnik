<?php

try {
    $message = 'Success';
    $result = calculate($_GET);
} catch (Exception $e) {
    $message = "Error: {$e->getMessage()}";
    $result = '';
}

echo json_encode([
    'result' => $result,
    'message' => $message,
], JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

function calculate(array $get)
{
    $courses = [
        'rur' => 0.013,
        'eur' => 1.21,
        'uah' => 0.036,
        'btc' => 51000,
    ];

    $count = $get['count'] ?? 0;
    $currency = $get['currency'] ?? '';

    if ($count <= 0 || !is_numeric($count)) {
        throw new Exception("Количество валюты (count) должно быть числом больше нуля");
    }

    $allowedCurrencies = array_keys($courses);
    if (!in_array($currency, $allowedCurrencies)) {
        throw new Exception("Валюта (currency) должна быть выбрана из списка: " . join(', ', $allowedCurrencies));
    }

    return number_format($count * $courses[$currency], 4);
}
