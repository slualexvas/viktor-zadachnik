<?php

function factorialRecursive(float $n)
{
    return $n == 0
        ? 1
        : $n * factorialRecursive($n - 1);
}