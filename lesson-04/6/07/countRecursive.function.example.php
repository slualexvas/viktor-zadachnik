<?php

function countRecursive($arr)
{
    if (!is_array($arr)) {
        return 1;
    } else {
        $result = 0;
        foreach ($arr as $value) {
            $result += countRecursive($value);
        }
        return $result;
    }
}