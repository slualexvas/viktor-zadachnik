<?php
function countRecursive ($array) {

    if(!is_array($array)) {
        return 1;
    }
    else {
        $result = 0;
        foreach ($array as $value) {
            $result += countRecursive($value);
        }
        return $result;
    }
}