<?php
require_once __DIR__ . '/../../BaseLesson4TestFunction.class.php';

class fibonacciTest extends BaseLesson4TestFunction
{
    public $functionNameTest = 'countRecursive';
    public $functionNameEtalon = self::FUNCTION_NAME_NOTNEED;

    public function argsProvider()
    {
        $testCases = [];

        $this->addTestCase($testCases, 3, [10, 20, 30]);
        $this->addTestCase($testCases, 0, []);
        $this->addTestCase($testCases, 5, [10, [20, 120, 220], 30]);
        $this->addTestCase($testCases, 2, [10, [], 30]);
        $this->addTestCase($testCases, 5, [10, [20, [30, [40, [50]]]]]);

        return $testCases;
    }
}