<?php

function myPow(float $base, int $exp)
{
    $result = 1;

    $i = 0;
    while ($i < $exp) {
        $result *= $base;
        $i++;
    }

    return $result;
}