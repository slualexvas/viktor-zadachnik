<?php

function echoComments($comments, $level = 0)
{
    foreach ($comments as $comment) {
        echo str_repeat("\t", $level) . "{$comment['login']}: {$comment['text']}\n";
        echoComments($comment['responses'], $level + 1);
    }
}