<?php

require_once __DIR__ . '/echoComments.function.example.php';

$comments = [
    [
        'id' => 1, 'login' => 'vasia', 'text' => 'Все бабы дуры', 'responses' => [
        [
            'id' => 2, 'login' => 'lena', 'text' => 'Вася, ты неправ! Женщины умные!', 'responses' => [
            ['id' => 3, 'login' => 'vasia', 'text' => "Ой, Лена, да иди ты на...", 'responses' => []],
            ['id' => 5, 'login' => 'ira', 'text' => "Правильно, Леночка! Так его!", 'responses' => []],
        ]
        ],
        ['id' => 4, 'login' => 'KABLUK', 'text' => 'Нет, не все бабы дуры! Моя жена умная', 'responses' => []],
    ]
    ],
    [
        'id' => 6, 'login' => 'buratino', 'text' => 'І слухай б-дь; от він бачить, що та х.ня летить', 'responses' => [
        [
            'id' => 7, 'login' => 'gurvinnik', 'text' => 'Та то якась х.ня. Не було такого', 'responses' => [
            ['id' => 8, 'login' => 'buratino', 'text' => 'Та яка х.ня! В книзі написано!', 'responses' => []],
        ]
        ]
    ]
    ]
];
echoComments($comments);