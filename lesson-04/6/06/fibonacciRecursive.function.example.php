<?php

function fibonacciRecursive(int $n)
{
    return ($n == 0 || $n == 1)
        ? $n
        : fibonacciRecursive($n - 1) + fibonacciRecursive($n - 2);
}