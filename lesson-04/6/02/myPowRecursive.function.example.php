<?php

function myPowRecursive(float $base, int $exp)
{
    return $exp == 0
        ? 1
        : $base * myPowRecursive($base, $exp - 1);
}