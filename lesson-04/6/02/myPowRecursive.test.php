<?php
require_once __DIR__ . '/../../BaseLesson4TestFunction.class.php';

class MyPowRecursiveTest extends BaseLesson4TestFunction
{
    public $functionNameTest = 'myPowRecursive';
    public $functionNameEtalon = 'pow';

    public function argsProvider()
    {
        $testCases = [];

        $this->addTestCase($testCases, 8, 2, 3);
        $this->addTestCase($testCases, 9, 3, 2);
        $this->addTestCase($testCases, 1, 4, 0);
        $this->addTestCase($testCases, 0, 0, 4);
        $this->addTestCase($testCases, 1.21, 1.1, 2);

        return $testCases;
    }
}