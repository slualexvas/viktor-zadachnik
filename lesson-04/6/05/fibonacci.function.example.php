<?php

function fibonacci(int $n)
{
    if ($n == 0 || $n == 1) {
        return $n;
    }

    $i = 2;
    $prev = 1; // предыдущее число
    $prevPrev = 0; // пред-предыдущее число
    while ($i <= $n) {
        $result = $prev + $prevPrev;
        $prevPrev = $prev;
        $prev = $result;
        $i++;
    }
    return $result;
}