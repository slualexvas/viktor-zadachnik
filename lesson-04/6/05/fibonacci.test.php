<?php
require_once __DIR__ . '/../../BaseLesson4TestFunction.class.php';

class fibonacciTest extends BaseLesson4TestFunction
{
    public $functionNameTest = 'fibonacci';
    public $functionNameEtalon = self::FUNCTION_NAME_NOTNEED;

    public function argsProvider()
    {
        $testCases = [];

        $this->addTestCase($testCases, 0, 0);
        $this->addTestCase($testCases, 1, 1);
        $this->addTestCase($testCases, 1, 2);
        $this->addTestCase($testCases, 2, 3);
        $this->addTestCase($testCases, 3, 4);
        $this->addTestCase($testCases, 5, 5);
        $this->addTestCase($testCases, 8, 6);

        return $testCases;
    }
}