<?php
require_once __DIR__ . '/../../BaseLesson4TestFunction.class.php';

class factorialTest extends BaseLesson4TestFunction
{
    public $functionNameTest = 'factorial';
    public $functionNameEtalon = self::FUNCTION_NAME_NOTNEED;

    public function argsProvider()
    {
        $testCases = [];

        $this->addTestCase($testCases, 2, 2);
        $this->addTestCase($testCases, 6, 3);
        $this->addTestCase($testCases, 24, 4);
        $this->addTestCase($testCases, 1, 0);

        return $testCases;
    }
}