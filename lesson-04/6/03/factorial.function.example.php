<?php

function factorial(float $n)
{
    $result = 1;

    $i = 1;
    while ($i <= $n) {
        $result *= $i;
        $i++;
    }

    return $result;
}