<?php
require_once __DIR__ . '/umax.function.php';

$players = [
    ['name' => 'Zinedin Zidan', 'price' => 46600000],
    ['name' => 'Вилли Гроувз', 'price' => 100],
    ['name' => 'Kaka', 'price' => 56000000],
    ['name' => 'Неймар', 'price' => 198000000],
    ['name' => 'Kristianu Ronaldu', 'price' => 80000000],
];

$biggestPricedPlayer = umax($players, /*здесь ваш код*/);
echo "Дороже всех -- {$biggestPricedPlayer['name']}\n";

//$smallestPricedPlayer = umax($players, /*здесь ваш код*/});
//echo "Дешевле всех -- {$smallestPricedPlayer['name']}\n";