<?php
require_once __DIR__ . '/umax.function.php';

/*
 * Пример:
 *
 * $strings = [
 *      'aa'
 *      'bbbbbb'
 *      'ccc'
 * ];
 *
 * Результат будет: 'bbbbbb'
 */
function getLongestString($strings)
{
    return umax($strings, function ($stringA, $stringB) {
        return mb_strlen($stringA) - mb_strlen($stringB);
    });
}