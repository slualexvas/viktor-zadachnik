<?php

// $cmpFunction -- это функция, которая сравнивает два значения и возвращает целое число
// Если функция вернула число 0 -- значит, значения "равны"
// Если функция вернула число > 0 -- значит, первое значение "больше" второго.
// Если функция вернула число < 0 -- значит, второе значение "больше" первого
function umax(array $arr, callable $cmpFunction)
{
    $result = reset($arr);

    foreach ($arr as $value) {
        if ($cmpFunction($result, $value) < 0) {
            $result = $value;
        }
    }

    return $result;
}