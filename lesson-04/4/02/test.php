<?php
require_once __DIR__ . '/../../BaseLesson4TestConsole.class.php';

class Test_4_4_01 extends BaseLesson4TestConsole
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getAddr()} [число] [знак +-*/] [число]\n";

        return [
            // Тесты на валидацию данных
//            ['2 + 3 + 4', "$usage"],
//            ['2 +', "$usage"],
//            ['a + 3', "$usage"],
//            ['3 + a', "$usage"],
//            ['3 % 2', "$usage"],

            // Тесты на знаки "+" и "-"
            ['2 + 0', "2"],
            ['0 + 2', "2"],
            ['3 + 4', "7"],
            ['3.1 + 4.5', "7.6"],
            ['5 - 2', "3"],

            // Тесты на знаки "*" и "/"
//            ['7 * 3', "21"],
//            ['0 * 3', "0"],
//            ['6 / 3', "2"],
//            ['0 / 3', "0"],
//            ['3 / 0', "на ноль делить нельзя"],
        ];
    }
}