<?php
require_once __DIR__ . '/../../1/03/mySum.function.example.php';

$usage = "Usage: {$argv[0]} [число] [знак +-*/] [число]\n";

$a = $argv[1] ?? null;
$b = $argv[3] ?? null;
$znak = $argv[2] ?? null;

$znakToFunctions = [
    '+' => 'mySum',
    '-' => function($a, $b) {return $a - $b;},
];

$f = $znakToFunctions[$znak];
$result = $f($a, $b);

echo "{$result}\n";