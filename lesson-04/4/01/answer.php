<?php
require_once __DIR__ . '/../../1/03/mySum.function.example.php';

$usage = "Usage: {$argv[0]} [число] [знак +-*/] [число]\n";

$a = $argv[1] ?? null;
$b = $argv[3] ?? null;
$znak = $argv[2] ?? null;

$znakToFuncNames = [
    '+' => 'mySum',
    '-' => 'myDifference',
];

$funcName = $znakToFuncNames[$znak];
$result = call_user_func($funcName, $a, $b);

echo "{$result}\n";

// Конец программы; дальше идут вспомогательные функции

function myDifference($a, $b)
{
    return $a - $b;
}