<?php
$players = [
    ['name' => 'Zinedin Zidan', 'price' => 46600000],
    ['name' => 'Вилли Гроувз', 'price' => 100],
    ['name' => 'Kaka', 'price' => 56000000],
    ['name' => 'Неймар', 'price' => 198000000],
    ['name' => 'Kristianu Ronaldu', 'price' => 80000000],
];

// Здесь кусок кода, который пересортировывает $players
printPlayers("Футболисты от дешёвых к дорогим", $players);

// Здесь кусок кода, который пересортировывает $players
printPlayers("Футболисты от дорогих к дешёвым", $players);

// Здесь кусок кода, который пересортировывает $players
printPlayers("Футболисты от коротких имён к длинным", $players);

// Здесь кусок кода, который пересортировывает $players
printPlayers("Футболисты от длинных имён к коротким", $players);

function printPlayers($textBefore, $players)
{
    $line = "\n-------------\n";

    echo "{$line}{$textBefore}:";
    foreach ($players as $player) {
        $formattedPrice = number_format($player['price']);
        echo "\n{$player['name']} ({$formattedPrice} фунтов)";
    }
    echo $line;
}