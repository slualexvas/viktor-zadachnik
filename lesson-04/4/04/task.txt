У вас есть файл "answer.php" с тем же списком футболистов, что и в предыдущем задании.

Ваша задача -- добиться того "php lesson-04/4/04/answer.php" выводил такой текст
(vendor/phpunit/phpunit/phpunit lesson-04/4/04/test.php):

-------------
Футболисты от дешёвых к дорогим:
Вилли Гроувз (100 фунтов)
Zinedin Zidan (46,600,000 фунтов)
Kaka (56,000,000 фунтов)
Kristianu Ronaldu (80,000,000 фунтов)
Неймар (198,000,000 фунтов)
-------------

-------------
Футболисты от дорогих к дешёвым:
Неймар (198,000,000 фунтов)
Kaka (56,000,000 фунтов)
Zinedin Zidan (46,600,000 фунтов)
Kristianu Ronaldu (80,000,000 фунтов)
Вилли Гроувз (100 фунтов)
-------------

-------------
Футболисты от коротких имён к длинным:
Kaka (56,000,000 фунтов)
Неймар (198,000,000 фунтов)
Вилли Гроувз (100 фунтов)
Zinedin Zidan (46,600,000 фунтов)
Kristianu Ronaldu (80,000,000 фунтов)
-------------

-------------
Футболисты от длинных имён к коротким:
Kristianu Ronaldu (80,000,000 фунтов)
Zinedin Zidan (46,600,000 фунтов)
Вилли Гроувз (100 фунтов)
Неймар (198,000,000 фунтов)
Kaka (56,000,000 фунтов)
-------------

