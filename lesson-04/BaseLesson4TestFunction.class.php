<?php

require __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

abstract class BaseLesson4TestFunction extends PHPUnit\Framework\TestCase
{
    public $functionNameEtalon = '';
    public $functionNameTest = '';

    const FUNCTION_NAME_NOTNEED = 'func-name-notneed';

    public function isExample()
    {
        global $argv;

        foreach ($argv as $key => $value) {
            if ($key != 0 && $value == 'example.php') {
                return true;
            }
        }

        return false;
    }

    public function requireFunctionFile()
    {
        $suffix = $this->isExample()
            ? 'function.example.php'
            : 'function.php';

        $reflector = new ReflectionClass(get_class($this));
        $dir = dirname($reflector->getFileName());

        require_once "$dir/{$this->functionNameTest}.{$suffix}";
    }

    public function setUp()
    {
        parent::setUp();
        $this->requireFunctionFile();
    }

    /**
     * @dataProvider argsProvider
     */
    public function testBasic($args, $result)
    {
        foreach (['functionNameEtalon', 'functionNameTest'] as $param) {
            $funcName = $this->$param;
            $this->assertNotEmpty($funcName, "Не установлен параметр '{$param}'!");
            if ($funcName == self::FUNCTION_NAME_NOTNEED) {
                continue;
            }

            $this->assertEquals(
                $result,
                call_user_func_array($funcName, $args),
                "\$result не совпадает с результатом {$funcName}"
            );
        }
    }

    abstract public function argsProvider();

    public function addTestCase(&$testCases, $result, ...$args)
    {
        $testCases[] = ['args' => $args, 'result' => $result];
    }
}
