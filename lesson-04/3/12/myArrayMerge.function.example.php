<?php

function myArrayMerge($first, $second)
{
    $result = $first;

    foreach ($second as $key => $value) {
        if (!isset($first[$key]) || is_numeric($key)) {
            $result[] = $value;
        } else {
            $result[$key] = $value;
        }
    }

    $renumberedResult = [];
    $newKey = 0;
    foreach ($result as $key => $value) {
        if (!is_numeric($key)) {
            $renumberedResult[$key] = $value;
        } else {
            $renumberedResult[$newKey] = $value;
            $newKey++;
        }
    }

    return $renumberedResult;
}