<?php

function myArrayProduct($array)
{
    $result = 1;

    foreach ($array as $value) {
        $result *= $value;
    }

    return $result;
}