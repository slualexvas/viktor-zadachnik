<?php

function myMin($array)
{
    $result = false;

    foreach ($array as $value) {
        if ($result === false || $value < $result) {
            $result = $value;
        }
    }

    return $result;
}