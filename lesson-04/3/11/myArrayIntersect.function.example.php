<?php

function myArrayIntersect($first, $second)
{
    $result = $first;

    foreach ($result as $key => $value) {
        if (!in_array($value, $second)) {
            unset($result[$key]);
        }
    }

    return $result;
}