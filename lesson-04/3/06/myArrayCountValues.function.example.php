<?php

function myArrayCountValues($array)
{
    $result = [];

    foreach ($array as $value) {
        $result[$value] = ($result[$value] ?? 0) + 1;
    }

    return $result;
}