<?php
require_once __DIR__ . '/../../BaseLesson4TestFunction.class.php';

class MyArraySumTest extends BaseLesson4TestFunction
{
    public $functionNameTest = 'myArraySum';
    public $functionNameEtalon = 'array_sum';

    public function argsProvider()
    {
        $testCases = [];

        $this->addTestCase($testCases, 5, [2, 3]);
        $this->addTestCase($testCases, 9, [1, 3, 5]);

        return $testCases;
    }
}