<?php

function myArraySum($array)
{
    $result = 0;

    foreach ($array as $value) {
        $result += $value;
    }

    return $result;
}