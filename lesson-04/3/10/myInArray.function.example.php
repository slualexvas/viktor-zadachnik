<?php

function myInArray($needle, $haystack, $strict = false)
{
    foreach ($haystack as $value) {
        if ($strict ? $value === $needle : $value == $needle) {
            return true;
        }
    }

    return false;
}