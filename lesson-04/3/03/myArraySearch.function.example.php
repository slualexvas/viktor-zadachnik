<?php

function myArraySearch($needle, $haystack, $strict = false)
{
    foreach ($haystack as $key => $value) {
        if ($strict ? $value === $needle : $value == $needle) {
            return $key;
        }
    }

    return false;
}