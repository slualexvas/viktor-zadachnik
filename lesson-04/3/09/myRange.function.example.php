<?php

// Вопрос "на подумать": я считаю, что эта функция написана коротко, но непонятно. Как её можно переписать длиннее, но понятней?
function myRange($start, $end, $step = 1)
{
    $result = [];

    $value = $start;
    while ($end < $start ? $value >= $end : $value <= $end) {
        $result[] = $value;
        $value += $end < $start ? -$step : $step;
    }

    return $result;
}