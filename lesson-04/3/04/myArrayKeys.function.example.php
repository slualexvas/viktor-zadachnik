<?php

function myArrayKeys($array, $searchValue = null, $strict = false)
{
    $result = [];

    foreach ($array as $key => $value) {
        if ($searchValue === null
        || ($strict ? $value === $searchValue : $value == $searchValue)) {
            $result[] = $key;
        }
    }

    return $result;
}