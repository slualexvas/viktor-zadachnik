<?php
function myIsInt($str)
{
    return is_numeric($str) && round($str) == $str;
}
