<?php
require_once __DIR__ . '/myIsInt.function.example.php';

$usage = "Usage: php {$argv[0]} [строка]\n";
if ($argc != 2) {
    die($usage);
}

echo myIsInt($argv[1]) ? 'целое' : 'нецелое';
echo "\n";