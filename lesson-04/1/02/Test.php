<?php
require_once __DIR__ . '/../../BaseLesson4TestConsole.class.php';

class Test_4_1_02 extends BaseLesson4TestConsole
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getCmd('')}[строка]\n";
        return [
            ["", $usage],
            ["1 2", $usage],
            ["1", "целое\n"],
            ["a", "нецелое\n"],
            ["1.5", "нецелое\n"],
        ];
    }

    public function testFileExists()
    {
        $example = $this->getFileName() == 'example.php' ? '.example' : '';
        $fileName = __DIR__ . "/myIsInt.function{$example}.php";

        $this->assertTrue(file_exists($fileName), "Не существует файл {$fileName}");
    }
}