<?php
require_once __DIR__ . '/../../BaseLesson4TestConsole.class.php';

class Test_4_1_01 extends BaseLesson4TestConsole
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getCmd('')}[строка]\n";
        return [
            ["", $usage],
            ["1 2", $usage],
            ["1", "целое\n"],
            ["a", "нецелое\n"],
            ["1.5", "нецелое\n"],
        ];
    }
}