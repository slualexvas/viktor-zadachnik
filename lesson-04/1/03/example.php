<?php
require_once __DIR__ . '/../02/myIsInt.function.example.php';
require_once __DIR__ . '/mySum.function.example.php';

$usage = "Usage: php {$argv[0]} [целое число] [целое число]\n";

// Сравни этот if с тем, что был в lesson-03/3/08/example.php
if ($argc != 3 || !myIsInt($argv[1]) || !myIsInt($argv[2])) {
    die($usage);
}

echo mySum($argv[1], $argv[2]);
echo "\n";