<?php
require_once __DIR__ . '/../../BaseLesson4TestConsole.class.php';

class Test_4_1_03 extends BaseLesson4TestConsole
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getCmd('')}[целое число] [целое число]\n";
        return [
            ["", $usage],
            ["1", $usage],
            ["-2 -3", "-5\n"],
            ["-2 a", $usage],
            ["2 1.5", $usage],
            ["2 3 7", $usage],
        ];
    }

    public function testFileExists()
    {
        $example = $this->getFileName() == 'example.php' ? '.example' : '';
        $fileName = __DIR__ . "/mySum.function{$example}.php";

        $this->assertTrue(file_exists($fileName), "Не существует файл {$fileName}");
    }

    public function testFileNotExists()
    {
        $example = $this->getFileName() == 'example.php' ? '.example' : '';
        $fileName = __DIR__ . "/myIsInt.function{$example}.php";

        $this->assertTrue(!file_exists($fileName), "Файл {$fileName} НЕ ДОЛЖЕН существовать");
    }
}