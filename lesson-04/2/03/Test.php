<?php
require_once __DIR__ . '/../../BaseLesson4TestConsole.class.php';

class Test_4_2_03 extends BaseLesson4TestConsole
{
    public function argsProvider()
    {
        $text = "<b>...<b>: The bold text
<i>...<i>: The italic text
<u>...<u>: The italic text
<code>...<code>: Text, formatted as program code
<del>...<del>: Text, formatted as deleted
<div>...<div>: The block container
<span>...<span>: The inline container
<h1>...<h1>: The header of 1st level
<h2>...<h2>: The header of 2nd level
<h3>...<h3>: The header of 3rd level
<h4>...<h4>: The header of 4th level
<h5>...<h5>: The header of 5th level
<h6>...<h6>: The header of 6th level
";
        return [
            ["", $text],
        ];
    }
}