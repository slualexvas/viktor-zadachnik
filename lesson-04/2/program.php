<?php
echo "<b>The bold text</b>\n";
echo "<i>The italic text</i>\n";
echo "<u>The italic text</u>\n";
echo "<code>Text, formatted as program code</code>\n";
echo "<del>Text, formatted as deleted</del>\n";
echo "<div>The block container</div>\n";
echo "<span>The inline container</span>\n";
echo "<h1>The header of 1st level</h1>\n";
echo "<h2>The header of 2nd level</h2>\n";
echo "<h3>The header of 3rd level</h3>\n";
echo "<h4>The header of 4th level</h4>\n";
echo "<h5>The header of 5th level</h5>\n";
echo "<h6>The header of 6th level</h6>\n";
