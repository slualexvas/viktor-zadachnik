<?php
function myEcho($tagName, $text) {
    echo "<{$tagName}>{$text}</{$tagName}>;\n";
}

myEcho('b', 'The bold text');
myEcho('i', 'The italic text');
myEcho('u', 'The italic text');
myEcho('code', 'Text, formatted as program code');
myEcho('del', 'Text, formatted as deleted');
myEcho('div', 'The block container');
myEcho('span', 'The inline container');
myEcho('h1', 'The header of 1st level');
myEcho('h2', 'The header of 2nd level');
myEcho('h3', 'The header of 3rd level');
myEcho('h4', 'The header of 4th level');
myEcho('h5', 'The header of 5th level');
myEcho('h6', 'The header of 6th level');