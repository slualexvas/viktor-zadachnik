<?php
require_once __DIR__ . '/../../BaseLesson4TestConsole.class.php';

class Test_4_2_01 extends BaseLesson4TestConsole
{
    public function argsProvider()
    {
        $text = "<b>The bold text</b>;
<i>The italic text</i>;
<u>The italic text</u>;
<code>Text, formatted as program code</code>;
<del>Text, formatted as deleted</del>;
<div>The block container</div>;
<span>The inline container</span>;
<h1>The header of 1st level</h1>;
<h2>The header of 2nd level</h2>;
<h3>The header of 3rd level</h3>;
<h4>The header of 4th level</h4>;
<h5>The header of 5th level</h5>;
<h6>The header of 6th level</h6>;
";
        return [
            ["", $text],
        ];
    }
}