<?php
require_once __DIR__ . '/../../BaseLesson4TestConsole.class.php';

class Test_4_2_01 extends BaseLesson4TestConsole
{
    public function argsProvider()
    {
        $text = "<b> --- The bold text
<i> --- The italic text
<u> --- The italic text
<code> --- Text, formatted as program code
<del> --- Text, formatted as deleted
<div> --- The block container
<span> --- The inline container
<h1> --- The header of 1st level
<h2> --- The header of 2nd level
<h3> --- The header of 3rd level
<h4> --- The header of 4th level
<h5> --- The header of 5th level
<h6> --- The header of 6th level
";
        return [
            ["", $text],
        ];
    }
}