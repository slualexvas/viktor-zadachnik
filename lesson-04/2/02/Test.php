<?php
require_once __DIR__ . '/../../BaseLesson4TestConsole.class.php';

class Test_4_2_02 extends BaseLesson4TestConsole
{
    public function argsProvider()
    {
        $text = "The bold text can be obtained as a result of tag 'b'
The italic text can be obtained as a result of tag 'i'
The italic text can be obtained as a result of tag 'u'
Text, formatted as program code can be obtained as a result of tag 'code'
Text, formatted as deleted can be obtained as a result of tag 'del'
The block container can be obtained as a result of tag 'div'
The inline container can be obtained as a result of tag 'span'
The header of 1st level can be obtained as a result of tag 'h1'
The header of 2nd level can be obtained as a result of tag 'h2'
The header of 3rd level can be obtained as a result of tag 'h3'
The header of 4th level can be obtained as a result of tag 'h4'
The header of 5th level can be obtained as a result of tag 'h5'
The header of 6th level can be obtained as a result of tag 'h6'
";
        return [
            ["", $text],
        ];
    }
}