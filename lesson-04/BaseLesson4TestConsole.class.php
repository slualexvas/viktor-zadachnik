<?php
require __DIR__ . '/../vendor/autoload.php';

abstract class BaseLesson4TestConsole extends PHPUnit\Framework\TestCase
{
    public function getFileName()
    {
        global $argv;

        foreach ($argv as $key => $value) {
            if ($key != 0 && $value == 'example.php') {
                return $value;
            }
        }

        return 'answer.php';
    }

    public function getAddr()
    {
        $reflector = new ReflectionClass(get_class($this));
        $dir = dirname($reflector->getFileName());
        return "{$dir}/{$this->getFileName()}";
    }

    public function getCmd($arg)
    {
        return "php {$this->getAddr()} {$arg}";
    }

    /**
     * @dataProvider argsProvider
     */
    public function testBasic($arg, $etalonData)
    {
        $testData = `{$this->getCmd($arg)}`;
        $this->assertEquals(trim($etalonData), trim($testData));
    }

    public function argsProvider()
    {
        return [
            ['', null],
        ];
    }
}
