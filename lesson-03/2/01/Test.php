<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_01 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["1 2", "1, 2\n"],
            ["1", "1\n"],
            ["Саша Петя Коля", "Саша, Петя, Коля\n"],
            ["", "\n"],
        ];
    }
}