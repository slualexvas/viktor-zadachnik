<?php

$result = [];
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    $result[] = $value;
}

echo implode(', ', $result) . "\n";