Код в цикле может выглядеть как-то так:

foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    $result[] = $value;
}