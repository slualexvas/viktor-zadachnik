<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_04 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["Саша купил хлеба", "Витя купил хлеба\n"],
            ["Витя купил хлеба", "Витя купил хлеба\n"],
            ["Саша Саша", "Витя Витя\n"],
            ["", "\n"],
        ];
    }
}