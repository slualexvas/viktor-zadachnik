<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_09 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["Александр", "Виктор\n"],
            ["Александр Александр", "Виктор Виктор\n"],
            ["Саша", "Витя\n"],
            ["Sasha", "Vitia\n"],
            ["Алехандро", "Викторио\n"],
            ["Viktoria", "Viktoria\n"],
            ["Саша Дима Александр Sasha Алехандро", "Витя Дима Виктор Vitia Викторио\n"],
            ["", "\n"],
        ];
    }
}