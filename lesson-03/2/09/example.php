<?php

$alexToViktor = [
    'Саша'      => 'Витя',
    'Sasha'     => 'Vitia',
    'Александр' => 'Виктор',
    'Алехандро' => 'Викторио',
];

$result = [];
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (isset($alexToViktor[$value])) {
        $value = $alexToViktor[$value];
    }

    $result[] = $value;
}

echo implode(' ', $result) . "\n";