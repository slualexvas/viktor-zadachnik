<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_08 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["Виктор", "Витя\n"],
            ["Виктор Виктор", "Витя Витя\n"],
            ["Викторио", "Витя\n"],
            ["Viktor", "Витя\n"],
            ["Viktorio", "Витя\n"],
            ["Viktoria", "Viktoria\n"],
            ["Виктор Дима Викторио Viktor Viktorio Viktoria", "Витя Дима Витя Витя Витя Viktoria\n"],
            ["", "\n"],
        ];
    }
}