<?php

$words = ['Виктор', 'Викторио', 'Viktor', 'Viktorio'];

$result = [];
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (in_array($value, $words)) {
        $value = 'Витя';
    }

    $result[] = $value;
}

echo implode(' ', $result) . "\n";