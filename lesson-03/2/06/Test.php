<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_06 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["Витя Коля Дима", "\n"],
            ["Дима Витя Коля", "Дима\n"],
            ["Дима Коля Витя", "Дима Коля\n"],
            ["Дима Коля Саша", "Дима Коля Саша\n"],
            ["Витя", "\n"],
            ["", "\n"],
        ];
    }
}