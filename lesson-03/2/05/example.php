<?php

$result = [];
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (is_numeric($value) && $value < 0) {
        $value = 0;
    }

    $result[] = $value;
}

echo implode(' ', $result) . "\n";