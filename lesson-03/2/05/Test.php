<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_05 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["-3", "0\n"],
            ["0", "0\n"],
            ["3", "3\n"],
            ["-2 -1 0 1 2", "0 0 0 1 2\n"],
            ["-3кг", "-3кг\n"],
            ["", "\n"],
        ];
    }
}