<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_07 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["Витя Коля Дима", "Витя\n"],
            ["Дима Витя Коля", "Дима Витя\n"],
            ["Дима Коля Витя", "Дима Коля Витя\n"],
            ["Дима Коля Саша", "Дима Коля Саша\n"],
            ["Витя", "Витя\n"],
            ["", "\n"],
        ];
    }
}