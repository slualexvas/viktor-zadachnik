<?php

$result = [];
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    $result[] = $value;

    if ($value == 'Витя') {
        break;
    }
}

echo implode(' ', $result) . "\n";