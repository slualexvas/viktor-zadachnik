<?php

$result = [];
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (mb_strlen($value) < 4) {
        $result[] = $value;
    }
}

echo implode(' ', $result) . "\n";