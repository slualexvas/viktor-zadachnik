<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_03 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["aaaaa bbbb ccc", "ccc\n"],
            ["ccccc", "\n"],
            ["", "\n"],
        ];
    }
}