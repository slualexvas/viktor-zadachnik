<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_2_02 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["2 3 4", "2 4\n"],
            ["1", "\n"],
            ["Саша Петя Коля", "\n"],
            ["", "\n"],
        ];
    }
}