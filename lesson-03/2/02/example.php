<?php

$result = [];
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (is_numeric($value) && round($value) == $value && $value % 2 == 0) {
        $result[] = $value;
    }
}

echo implode(' ', $result) . "\n";