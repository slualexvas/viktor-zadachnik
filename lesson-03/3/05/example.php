<?php
$usage = "Usage: php {$argv[0]} [целое число больше нуля]\n";

if ($argc != 2 || !is_numeric($argv[1]) || round($argv[1]) != $argv[1] || $argv[1] <= 0) {
    die($usage);
}

$i = 1;
while ($i < 10) {
    $result = $argv[1] * $i;
    echo "{$argv[1]} * {$i} = {$result}\n";
    $i++;
}
