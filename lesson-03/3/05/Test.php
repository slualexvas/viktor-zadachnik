<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_3_04 extends BaseLesson3Test
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getCmd('')}[целое число больше нуля]\n";

        return [
            ["", $usage],
            ["1 2", $usage],
            ["a", $usage],
            ["1.5", $usage],
            ["-3", $usage],
            ["0", $usage],
            ["6", "6 * 1 = 6
6 * 2 = 12
6 * 3 = 18
6 * 4 = 24
6 * 5 = 30
6 * 6 = 36
6 * 7 = 42
6 * 8 = 48
6 * 9 = 54
"],
        ];
    }
}