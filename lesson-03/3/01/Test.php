<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_3_01 extends BaseLesson3Test
{
    public function argsProvider()
    {
        $result = '';

        $i = 1;
        while ($i < 10) {
            $result .= "{$i} ";
            $i = $i + 2;
        }
        $result .= "\n";

        return [
            ["", $result],
        ];
    }
}