<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_3_04 extends BaseLesson3Test
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getCmd('')}[целое число больше нуля]\n";

        return [
            ["", $usage],
            ["1 2", $usage],
            ["a", $usage],
            ["1.5", $usage],
            ["-3", $usage],
            ["0", $usage],
            ["1", "\n"],
            ["9", "2 4 6 8 \n"],
            ["8", "2 4 6 8 \n"],
        ];
    }
}