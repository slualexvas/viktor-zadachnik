<?php
$usage = "Usage: php {$argv[0]} [целое число больше нуля]\n";

if ($argc != 2 || !is_numeric($argv[1]) || round($argv[1]) != $argv[1] || $argv[1] <= 0) {
    die($usage);
}

$i = 2;
while ($i <= $argv[1]) {
    echo "{$i} ";
    $i += 2;
}
echo "\n";