<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_3_02 extends BaseLesson3Test
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getCmd('')}[целое число больше нуля]\n";

        return [
            ["", $usage],
            ["1 2", $usage],
            ["a", $usage],
            ["1.5", $usage],
            ["-3", $usage],
            ["0", $usage],
            ["2", "1 2 \n"],
            ["5", "1 2 3 4 5 \n"],
        ];
    }
}