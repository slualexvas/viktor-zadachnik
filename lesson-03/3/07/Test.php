<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_3_07 extends BaseLesson3Test
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getCmd('')}[целое число А] [целое число B >= A]\n";

        return [
            ["", $usage],
            ["1", $usage],
            ["a", $usage],
            ["1.5", $usage],
            ["1 a", $usage],
            ["1 1.5", $usage],
            ["1 0", $usage],
            ["1 2 3", $usage],
            ["1 1", "1 \n"],
            ["1 2", "1 2 \n"],
            ["1 3", "1 2 3 \n"],
            ["-4 0", "-4 -3 -2 -1 0 \n"],
            ["-4 4", "-4 -3 -2 -1 0 \n"],
        ];
    }
}