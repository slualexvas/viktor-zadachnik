<?php
$usage = "Usage: php {$argv[0]} [целое число А] [целое число B >= A]\n";

if ($argc != 3
    || !is_numeric($argv[1]) || round($argv[1]) != $argv[1]
    || !is_numeric($argv[2]) || round($argv[2]) != $argv[2] || $argv[2] < $argv[1]) {
    die($usage);
}

$i = $argv[1];
while ($i <= $argv[2]) {
    echo "{$i} ";
    if ($i == 0) {
        break;
    }
    $i++;
}
echo "\n";