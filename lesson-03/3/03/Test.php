<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_3_03 extends BaseLesson3Test
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getCmd('')}[целое число меньше нуля]\n";

        return [
            ["", $usage],
            ["1 2", $usage],
            ["a", $usage],
            ["1.5", $usage],
            ["3", $usage],
            ["0", $usage],
            ["-1", "0 -1 \n"],
            ["-3", "0 -1 -2 -3 \n"],
        ];
    }
}