<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_08 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["2 101 201", "1\n"],
            ["101 2 201", "2\n"],
            ["101 201 2", "3\n"],
            ["2 4 6", "3\n"],
            ["1.5", "Нету\n"],
            ["А", "Нету\n"],
            ["", "Нету\n"],
        ];
    }
}