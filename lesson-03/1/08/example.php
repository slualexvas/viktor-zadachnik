<?php

$result = "Нету";
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (is_numeric($value) && $value == round($value) && $value % 2 == 0) {
        $result = $key;
    }
}

echo "{$result}\n";