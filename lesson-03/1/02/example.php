<?php

$result = false;
foreach ($argv as $key => $value) {
    if ($key != 0 && is_numeric($value) && $value == round($value) && $value % 2 == 0) {
        $result = true;
    }
}

echo ($result ? 'Да' : 'Нет') . "\n";