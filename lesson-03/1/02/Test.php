<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_02 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["1 2 3", "Да\n"],
            ["1 3 5", "Нет\n"],
            ["1.5", "Нет\n"],
            ["A", "Нет\n"],
            ["", "Нет\n"],
        ];
    }
}