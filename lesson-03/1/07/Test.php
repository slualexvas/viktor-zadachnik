<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_07 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["Коля Витя Петя Витя", "4\n"],
            ["Коля Витя", "2\n"],
            ["Витя Коля", "1\n"],
            ["Коля Петя", "Нету\n"],
            ["", "Нету\n"],
        ];
    }
}