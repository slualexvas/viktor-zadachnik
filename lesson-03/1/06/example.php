<?php

$result = "Нету";
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (is_numeric($value) && $value == round($value) && $value % 2 == 0) {
        $result = $key;
        break; // как только нашли первое чётное число -- сразу же останавливаемся
    }
}

echo "{$result}\n";