<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_03 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["2 4", "Да\n"],
            ["2 3", "Нет\n"],
            ["3 2", "Нет\n"],
            ["1.5", "Нет\n"],
            ["A", "Нет\n"],
            ["", "Да\n"],
        ];
    }
}