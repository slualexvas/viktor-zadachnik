<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_01 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["Саша Витя Коля", "Да\n"],
            ["Саша Коля Витя", "Да\n"],
            ["Саша Петя Коля", "Нет\n"],
            ["Саша Коля Петя", "Нет\n"],
            ["", "Нет\n"],
        ];
    }
}