<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_10 extends BaseLesson3Test
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getAddr()} [число] [число] ...\n";
        return [
            ["2", "2\n"],
            ["1 0", "0\n"],
            ["1.5 -2", "-3\n"],
            ["2 a 10", $usage],
            ["a b", $usage],
            ["", $usage],
        ];
    }
}