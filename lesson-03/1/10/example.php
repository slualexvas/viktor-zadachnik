<?php

$usage = "Usage: {$argv[0]} [число] [число] ...\n";
if ($argc == 1) {
    die($usage);
}

$result = 1;
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (!is_numeric($value)) {
        die($usage);
    }

    $result *= $value;
}
echo "{$result}\n";