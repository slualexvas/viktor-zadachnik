<?php

$result = true;
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (mb_strlen($value) >= 4) {
        $result = false;
    }
}

echo ($result ? 'Да' : 'Нет') . "\n";