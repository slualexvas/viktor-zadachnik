<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_04 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["А БББ", "Да\n"],
            ["АААА Б", "Нет\n"],
            ["", "Да\n"],
        ];
    }
}