<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_09 extends BaseLesson3Test
{
    public function argsProvider()
    {
        $usage = "Usage: {$this->getAddr()} [число] [число] ...\n";
        return [
            ["1 10", "11\n"],
            ["1.7 2.9", "4.6\n"],
            ["1 0 -100 0 200", "101\n"],
            ["a b", $usage],
            ["", $usage],
        ];
    }
}