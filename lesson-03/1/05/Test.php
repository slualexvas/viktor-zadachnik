<?php
require_once __DIR__ . '/../../BaseLesson3Test.class.php';

class Test_1_05 extends BaseLesson3Test
{
    public function argsProvider()
    {
        return [
            ["Витя Петя Коля", "1\n"],
            ["Коля Витя Петя", "2\n"],
            ["Петя Коля Витя", "3\n"],
            ["Витя Витя Витя", "1\n"],
            ["Коля Миша Витя Петя Витя Вася", "3\n"],
            ["", "Нету\n"],
        ];
    }
}