<?php

$usage = "Usage: {$argv[0]} [число] [число] ...\n";

if ($argc == 1) {
    die($usage);
}

$sum = 0;
$count = 0;
foreach ($argv as $key => $value) {
    if ($key == 0) {
        continue;
    }

    if (!is_numeric($value)) {
        die($usage);
    }

    $sum += $value;
    $count++;
}

echo ($sum / $count) . "\n";