<?php

class Point
{
    public $x;
    public $y;
    public $name;

    public function __construct($x, $y, $name)
    {
        $this->x = $x;
        $this->y = $y;
        $this->name = $name;
        if (!is_numeric($x) || !is_numeric($y)) {
            die("Значение координаты должно быть числом\n");
        }
    }

    public function print()
    {
        if (!is_numeric($this->x) || !is_numeric($this->y)) {
            die("Значение координаты должно быть числом\n");
        }
        echo "{$this->name}({$this->x};{$this->y}).\n";
    }
}