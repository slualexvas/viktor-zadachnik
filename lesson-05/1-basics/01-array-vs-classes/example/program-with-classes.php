<?php

require_once __DIR__ . '/Point.class.php';

$point1 = new Point(0, 0, 'A');
$point2 = new Point(10, 20, 'B');
$point3 = new Point(5, 3, 'C');
$point3->x = 'afa';

$point1->print();
$point2->print();
$point3->print();