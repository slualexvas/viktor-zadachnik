<?php

$worker1 = ['name' => 'Александр', 'salary' => 150, 'workdays' => 20];
$worker2 = ['name' => 'Виктор', 'salary' => 200, 'workdays' => 15];

echo "Сотрудник: {$worker1['name']}; Зп за месяц: " . ($worker1['salary'] * $worker1['workdays']) . "\n";
echo "Сотрудник: {$worker2['name']}; Зп за месяц: " . ($worker2['salary'] * $worker2['workdays']) . "\n";
