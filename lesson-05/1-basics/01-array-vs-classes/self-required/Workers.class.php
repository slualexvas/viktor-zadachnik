<?php

class Workers
{
    public $name;
    public $salary;
    public $workdays;

    public function __construct($name, $salary, $workdays)
    {
        $this->name = $name;
        $this->salary = $salary;
        $this->workdays = $workdays;
    }

    public function print()
    {
        echo "Сотрудник: {$this->name}; Зп за месяц: " . ($this->salary * $this->workdays) . "\n";
    }
}