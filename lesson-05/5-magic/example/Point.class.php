<?php

class Point
{
    private $x;
    private $y;
    private $name;

    public function __construct($x, $y, $name)
    {
        $this->x = $x;
        $this->y = $y;
        $this->name = $name;
    }

    // Например, "A(2;3)"
    public function __toString()
    {
        return "{$this->name}({$this->x};{$this->y})";
    }

    public function __set($name, $value)
    {
        switch ($name) {
            case 'x':
            case 'y':
                if (!is_numeric($value)) {
                    throw new Exception("Координата {$name} должна быть числом");
                }
            case 'name':
                $this->$name = $value;
                break;
            default:
                throw new Exception("Некорректное свойство (надо x, y, или name)");
                break;
        }
    }

    public function __get($name)
    {
        return $this->$name;
    }
}