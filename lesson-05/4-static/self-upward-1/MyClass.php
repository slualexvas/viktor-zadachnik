<?php

class MyClass
{
    private $counter = 1;

    public function myFunction()
    {
        echo ($this->counter++) . "-й вызов функции\n";
    }
}