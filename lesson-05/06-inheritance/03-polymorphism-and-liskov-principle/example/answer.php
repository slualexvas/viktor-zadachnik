<?php
require_once __DIR__ . '/Dog.class.php';
//require_once __DIR__ . '/Cat.class.php';

$data = [
    ['dog', 'Тобик'],
    ['dog', 'Мухтар'],
//    ['cat', 'Ричик'],
//    ['cat', 'Мурзик'],
];

foreach ($data as $row)
{
    $dog = new Dog($row[1]);
    dance($dog);
}

function dance(Dog $dog)
{
    echo "\nТанцует {$dog->getName()}!\n";
    $dog->run();
    $dog->jump();
    $dog->run();
}