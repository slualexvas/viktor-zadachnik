<?php

class Animal
{
    protected $type = 'Животное';
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function run()
    {
        echo "{$this} бежит\n";
    }

    public function jump()
    {
        echo "{$this} прыгает\n";
    }

    public function __toString()
    {
        return "{$this->type} {$this->name}";
    }
}