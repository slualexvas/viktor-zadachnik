<?php
require_once __DIR__ . '/User.class.php';
require_once __DIR__ . '/Admin.class.php';

$vasia = new User("Вася");
$petia = new Admin("Петя");

$vasia->login(); // echo "Вася залогинился"
$petia->login(); // echo "Петя залогинился"

$vasia->printAllowedActions(); // Логин, логаут
$petia->printAllowedActions(); // Логин, логаут, администрирование