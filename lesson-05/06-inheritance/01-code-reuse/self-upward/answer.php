<?php

require_once __DIR__ . '/Dog.class.php';
require_once __DIR__ . '/Duck.class.php';
require_once __DIR__ . '/Chicken.class.php';

$dog = new Dog("Тобик");
$dog->voice(); // echo "Собака Тобик говорит ГАВ"
$dog->run(); // echo "Собака Тобик бежит по земле"

$duck = new Duck("Матильда");
$duck->voice(); // echo "Утка Матильда говорит КРЯ"
$duck->run(); // echo "Утка Матильда бежит по земле"
$duck->swim(); // echo "Утка Матильда плывёт по воде"
$duck->fly(); // echo "Утка Матильда летит по воздуху"

$chicken = new Chicken("Гамбургский");
$chicken->voice(); // echo "Петух Гамбургский говорит КУКАРЕКУ"
$chicken->run(); // echo "Петух Гамбургский бежит по земле"
$chicken->fly(); // echo "Петух Гамбургский летит по воздуху"
