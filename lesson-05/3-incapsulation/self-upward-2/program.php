<?php

require_once __DIR__ . 'Product.class.php';

$product = new Product('Телефон');
$product->setPriceUAH(3000);
$product->print(); // Должно вывести: "Товар 'Телефон'. Цена = 3000 UAH"

$product->setCourses(['USD' => 25, 'EUR' => 30]);
$product->print(); // Должно вывести: "Товар 'Телефон'. Цена = 3000 UAH, 120 USD, 100 EUR"

$product->setCourses(['RUR' => 0.5]);
$product->print(); // Должно вывести: "Товар 'Телефон'. Цена = 3000 UAH, 6000 RUR"


