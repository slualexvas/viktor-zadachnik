<?php

require_once __DIR__ . 'Product.class.php';

$product = new Product('Телефон');
$product->setPriceUSD(100);
echo "Товар '{$product->getName()}'. Цена = {$product->getPriceUAH()} UAH, {$product->getPriceUSD()} USD.\n";