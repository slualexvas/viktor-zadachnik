<?php

class Product
{
    // курс доллара
    const RATIO = 26;

    private $priceUAH;
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->priceUAH = 0;
    }

    public function setPriceUAH(float $newPrice)
    {
        $this->priceUAH = $newPrice;
    }

    public function setPriceUSD(float $newPrice)
    {
        $this->priceUAH = $newPrice / self::RATIO;
    }

    public function getPriceUAH()
    {
        return $this->priceUAH;
    }

    public function getPriceUSD()
    {
        return $this->priceUAH * self::RATIO;
    }

    public function getName()
    {
        return $this->name;
    }
}