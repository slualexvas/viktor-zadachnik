<?php
require_once __DIR__ . '/DB/DbOperations.php';

$db = new DbOperations(
    'mysql:host=mysql316.1gb.ua; dbname=gbua_viktor77',
    'gbua_viktor77',
    'b33b3fe9gh'
);



/** Должно быть:
 * [
 *  ['id' => 1, 'name' => 'one'],
 *  ['id' => 2, 'name' => 'two'],
 *  ['id' => 3, 'name' => 'three'],
 * ]
 */
$sql = "SELECT * FROM one";
var_dump($db->fetchAll($sql));

/** Должно быть: [1, 2, 3] */
//var_dump($db->fetchCol($sql, 1));
//var_dump($db->fetchCol("SELECT id, name FROM one"));

///** Должно быть: ['id' => 1, 'name' => 'one'] */
//var_dump($db->fetchRow($sql));
//
///** Должно быть: 1 */
//var_dump($db->fetchOne($sql));