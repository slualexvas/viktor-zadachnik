<?php
require_once __DIR__ . '/Counter.php';

class CounterTest extends \PHPUnit\Framework\TestCase
{
    private Counter $counter;

    public function setUp()
    {
        parent::setUp();
        $this->counter = new Counter();
        $this->counter->clean();
    }

    public function testFirst()
    {
        $this->assertEquals(0, $this->counter->getValue());
    }

    public function testSecond()
    {
        $this->counter->increment();
        $this->assertEquals(1, $this->counter->getValue());
    }
}