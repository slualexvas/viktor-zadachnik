<?php

class Counter
{
    private $filePath = '/tmp/counter.txt';

    private function setValue(int $value)
    {
        file_put_contents($this->filePath, $value);
    }

    public function getValue()
    {
        return file_exists($this->filePath)
            ? file_get_contents($this->filePath)
            : 0;
    }

    public function increment()
    {
        $value = $this->getValue();
        $value++;
        $this->setValue($value);
    }

    public function clean()
    {
        if (file_exists($this->filePath)) {
            unlink($this->filePath);
        }
    }
}