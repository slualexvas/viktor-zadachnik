<?php

class FibonacciCalculator
{
    public function calculate(int $n)
    {
        if ($n == 1) {
            return 1;
        }

        if ($n == 2) {
            return 2;
        }

        return $this->calculate($n - 1) + $this->calculate($n - 2);
    }
}
