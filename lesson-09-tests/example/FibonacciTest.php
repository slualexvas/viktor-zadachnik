<?php
require_once __DIR__ . '/fibonacci.function.php';

class FibonacciTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param int $n
     * @param int $etalonResult
     *
     * @dataProvider myProvider
     */
    public function testFirst(int $n, int $etalonResult)
    {
        $fibonacciCalculator = new FibonacciCalculator();
        $testResult = $fibonacciCalculator->calculate($n);
        $this->assertEquals($etalonResult, $testResult);
    }

    public function myProvider()
    {
        return [
            [3, 3],
            [4, 5],
            [10, 89],
            [20, 10946],
            [35, 14930352],
        ];
    }
}