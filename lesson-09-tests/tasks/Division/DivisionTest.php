<?php
require_once __DIR__ . '/Division.php';

class DivisionTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param int $a
     * @param int $b
     * @param int $etalonResult
     * @throws Exception
     *
     * @dataProvider myProvider
     */
    public function testFirst(int $a, int $b, $etalonResult)
    {
        $testResult = division($a, $b);
        $this->assertEquals($etalonResult, $testResult);
    }

    public function myProvider()
    {
        return [
            [12, 4, 3],
            [20, -2, -10],
            [-10, 5, -2],
            [0, 2, 0],
            [1, 1, 1],
        ];
    }

    public function testDivisionZeroWithTryCatch()
    {
        try {
            division(1, 0);
        } catch (Exception $e)
        {
            $message = $e->getMessage();
        }
        $this->assertEquals('Делить на 0 нельзя', $message);
    }

    public function testDivisionZeroWithoutTryCatch()
    {
        $this->expectExceptionMessage('Делить на 0 нельзя');
        division(1, 0);
    }
}