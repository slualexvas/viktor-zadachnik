<?php

function division(int $a, int $b)
{
    if($b == 0)
    {
        throw new Exception("Делить на 0 нельзя");
    }
    return $a / $b;
}

