<?php

function getPdoDsn($host, $user, $password, $dbName, $port = 0)
{
    return "{$user}:{$password}@{$host}/{$dbName}" . (!empty($port) ? ":{$port}" : '');
}

function definePdoDsn($host, $user, $password, $dbName, $port = 0)
{
    define('DSN', getPdoDsn($host, $user, $password, $dbName, $port));
}