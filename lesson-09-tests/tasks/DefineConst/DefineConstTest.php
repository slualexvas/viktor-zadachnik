<?php
require_once __DIR__ . '/DefineConst.php';

class DefineConstTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param $host
     * @param $user
     * @param $password
     * @param $dbName
     * @param int $port
     * @param $etalonResult
     *
     * @dataProvider myProvider
     */
    public function testFirst($host, $user, $password, $dbName, $port, $etalonResult)
    {
        $testResult = getPdoDsn($host, $user, $password, $dbName, $port);

        $this->assertEquals($etalonResult, $testResult);
    }

    public function myProvider()
    {
        return [
            ['localhost', 'vasia', 'my_password', 'my_db', '5432', 'vasia:my_password@localhost/my_db:5432'],
            ['localhost', 'vasia', 'my_password', 'my_db', '0', 'vasia:my_password@localhost/my_db'],
        ];
    }
}