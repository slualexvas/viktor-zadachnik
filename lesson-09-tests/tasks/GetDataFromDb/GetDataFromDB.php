<?php
require_once __DIR__ . '/db/DbDecorator.class.php';

class GetDataFromDB
{
    private $db;

    public function __construct()
    {
        $this->db = DbDecorator::getInstance();
    }

    public function getDataFromDb(string $tableName, array $columnNames): array
    {
        // $columns - временная переменная, в которую добавляются имена столбцов таблицы для запроса SQL.
        $columns = "";

        $columns = join(", ", $columnNames);
        return $this->db->fetchAll("SELECT {$columns} FROM {$tableName};");
    }
}