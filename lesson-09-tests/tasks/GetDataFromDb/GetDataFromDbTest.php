<?php
require_once __DIR__ . '/GetDataFromDB.php';
require_once __DIR__ . '/db/DbDecorator.class.php';

class GetDataFromDbTest extends \PHPUnit\Framework\TestCase
{
    private GetDataFromDB $getDataFromDb;
    private $db;

    public function setUp()
    {
        $this->db = DbDecorator::getInstance();
        $this->getDataFromDb = new GetDataFromDB();
        parent::setUp();
    }

    public function makeTable()
    {
        $sql = "DROP TABLE IF EXISTS workers";
        $this->db->exec($sql);

        $sql = "CREATE TABLE workers(
          `id` int,
          `name` VARCHAR(20),
          `lastName` VARCHAR(30)
          )";
        $this->db->exec($sql);
    }

    public function testFirst()
    {
        $this->makeTable();
        $sql = "INSERT INTO workers VALUES
            (1, 'Вася', 'Иванов'),
            (2, 'Петя' , 'Сидоров')";
        $this->db->exec($sql);

        $etalonResult = [
            [
                'id'   => 1,
                'name' => 'Вася',
            ],
            [
                'id'   => 2,
                'name' => 'Петя',
            ],
        ];
        $testResult = $this->getDataFromDb->getDataFromDb('workers', [
            'id',
            'name',
        ]);

        $this->assertEquals($etalonResult, $testResult);
    }

    public function testSecond()
    {
        $sql = "DELETE FROM workers WHERE TRUE;";
        $this->db->exec($sql);

        $sql = "INSERT INTO workers VALUES
            (1, 'Андрей', 'Иванов'),
            (2, 'Петя' , 'Сидоров')";
        $this->db->exec($sql);

        $etalonResult = [
            [
                'id'       => 1,
                'name'     => 'Андрей',
                'lastname' => 'Иванов',
            ],
            [
                'id'       => 2,
                'name'     => 'Петя',
                'lastname' => 'Сидоров',
            ],
        ];
        $testResult = $this->getDataFromDb->getDataFromDb('workers', [
            'id',
            'name',
            'lastname',
        ]);
        $this->assertEquals($etalonResult, $testResult);
    }
}