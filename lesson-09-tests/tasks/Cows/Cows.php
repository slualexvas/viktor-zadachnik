<?php

function declension(int $n)
{
    $res = "";
    if(in_array($n, [11, 12, 13, 14]))
    {
        return "коров";
    }

    $divider = $n % 10;
    switch ($divider) {
        case 1:
            $res = "корова";
            break;
        case 2:
        case 3:
        case 4:
            $res = "коровы";
            break;
        default:
            $res = "коров";
    }
    return $res;
}