<?php
require_once __DIR__ . '/Cows.php';

class CowsTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param int $n
     * @param string $etalonResult
     *
     * @dataProvider myDataProvider
     */
    public function testFirst(int $n, string $etalonResult)
    {
        $testResult = declension($n);
        $this->assertEquals($etalonResult, $testResult);
    }

    public function myDataProvider()
    {
        return [
            [1, "корова"],
            [3, "коровы"],
            [33, "коровы"],
            [4, "коровы"],
            [5, "коров"],
            [14, "коров"],
            [0, "коров"],
            [11, "коров"],
            [12, "коров"],
        ];
    }
}