<?php
require_once __DIR__ . "/Dates.php";

class DatesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param string $firstDate
     * @param string $secondDate
     * @param array $etalonResult
     *
     * @dataProvider myProvider
     */


    public function testFirst(string $firstDate, string $secondDate, array $etalonResult)
    {
        $testResult = holiday($firstDate, $secondDate);
        $this->assertEquals($etalonResult, $testResult);
    }

    public function myProvider()
    {
        return [
            [
                '2021-01-08',
                '2021-01-09',
                ['2021-01-08' => true, '2021-01-09' => false],
            ],
            [
                '2021-01-10',
                '2021-01-11',
                ['2021-01-10' => false, '2021-01-11' => true],
            ],
        ];
    }
}