<?php
function getWeekday($date) {
    return date('w', strtotime($date));
}

function holiday(string $firstDate, string $secondDate): array
{
    $dates = [$firstDate, $secondDate];

// $result - это результат, соответствующий залданной дате. Если день 6(суббота) или 0(воскресенье), то $result будет false. В других случаях(для будних дней) $result будет true.
    $result = [];
    foreach ($dates as $date)
    {
        in_array(getWeekday($date), [0, 6])
            ? $result[$date] = false
            : $result[$date] = true;
    }

    return $result;
}


