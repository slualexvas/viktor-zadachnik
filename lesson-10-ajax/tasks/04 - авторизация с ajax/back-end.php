<?php

try {
    authorize($_REQUEST['login'] ?? '', $_REQUEST['password'] ?? '');
    $result = [
        'status' => true,
        'message' => 'Вы успешно авторизованы',
    ];
} catch (Exception $e) {
    $result = [
        'status' => false,
        'message' => $e->getMessage(),
    ];
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);


function authorize($login, $password)
{
    if ($login == 'admin' && $password == '1234') {
        return true;
    } else if (empty($login)) {
        throw new Exception("Не указан логин");
    } else if (empty($password)) {
        throw new Exception("Не указан пароль");
    } else {
        throw new Exception("Неверный логин или пароль");
    }
}