<?php
require_once __DIR__ . '/View.class.php';
require_once __DIR__ . '/Model.class.php';

class Controller
{
    private $model;
    private $view;

    public function __construct()
    {
        $this->model = new Model();
        $this->view = new View();
    }

    public function actionIndex()
    {
        $this->view->data = $this->model->getDataFromDb();
        $this->view->echoPage();
    }

    public function actionCreateRow()
    {
        $this->model->createRow();
        $this->actionIndex();
    }

    public function actionEditRow()
    {
        $this->model->editRow();
        $this->actionIndex();
    }

    public function actionDeleteRow()
    {
        $this->model->deleteRow();
        $this->actionIndex();
    }

    public function actionRecreateDb()
    {
        $this->model->recreateDb();
        $this->actionIndex();
    }
}