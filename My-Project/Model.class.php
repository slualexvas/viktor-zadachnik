<?php
require_once __DIR__ . '/View.class.php';
require_once __DIR__ . '/Controller.class.php';
require_once __DIR__ . '/DB/DbOperations.php';

class Model
{
    private $db;

    public function __construct()
    {
        $this->db = new DbOperations(
            'mysql:host=mysql316.1gb.ua; dbname=gbua_viktor77',
            'gbua_viktor77',
            'b33b3fe9gh'
        );
    }

    public function getDataFromDb()
    {
        $sql = "SELECT * FROM one";
        return $this->db->fetchAll($sql);
    }

    public function createRow()
    {
        $nameValue = $_POST['row_name'];
        $statusValue = $_POST['status'];
        if(empty($nameValue))
        {
            throw new Exception("Название строки не может быть пустым");
        }
        if($statusValue != 0 && $statusValue != 1)
        {
            throw new Exception("Статус строки может быть ТОЛЬКО 0 или 1!");
        }
        $sql = "INSERT INTO one(`name`, `status`) VALUES ('{$nameValue}', '{$statusValue}')";
        $this->db->exec($sql);
    }

    public function editRow()
    {
        $newData = $_POST;

        if(empty($newData['row_id']))
        {
            throw new Exception("Укажите номер изменяемой строки");
        }
        if(empty($newData['new_row_name']))
        {
            throw new Exception("Название новой строки не может быть пустым");
        }
        $sql = "UPDATE one
        SET `name` = {$this->db->quote($newData['new_row_name'])},
            `status` = {$this->db->quote($newData['status'])}
        WHERE id = {$newData['row_id']}";
        $this->db->exec($sql);
    }

    public function deleteRow()
    {
        if(empty($_POST['delete_row_id']))
        {
            throw new Exception("Номер удаляемой строки не может быть пустым");
        }
        $sql = "DELETE FROM one WHERE id = {$_POST['delete_row_id']}";
        $this->db->exec($sql);
    }

    public function recreateDb()
    {
        $sql = "DROP TABLE IF EXISTS one";
        $this->db->exec($sql);

        $sql = "CREATE TABLE one(
        `id` INT NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(15),
        `status` VARCHAR(1),
                PRIMARY KEY (id)
)";
        $this->db->exec($sql);

        $sql = "INSERT INTO one VALUES 
                       (1, 'one', '0'),
                       (2, 'two', '1'),
                       (3, 'three', '0')";
        $this->db->exec($sql);
    }
}