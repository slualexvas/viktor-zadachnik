<?php
require_once __DIR__ . '/Controller.class.php';
require_once __DIR__ . '/View.class.php';

$controller = new Controller();
$view = new View();

//echo "GET:"; var_dump($_GET); echo "<br>";
//echo "POST:"; var_dump($_POST); echo "<br>";
//echo "REQUEST:"; var_dump($_REQUEST); echo "<br>";
try {
    $action = $_REQUEST['action'] ?? 'index';
    switch ($action) {
        case 'index':
            $controller->actionIndex();
            break;
        case 'create_row':
            $controller->actionCreateRow();
            break;
        case 'edit_row':
            $controller->actionEditRow();
            break;
        case 'delete_row':
            $controller->actionDeleteRow();
            break;
        case 'recreate_table':
            $controller->actionRecreateDb();
            break;
        default:
            break;
    }
} catch (Exception $e)
{
    $view->errorPage($e);
}