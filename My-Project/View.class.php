<?php

class View
{
    public array $data;
//    public array $checkedIds;

    public function echoPage()
    {
        echo "
<html>
<head>
    <meta charset='UTF-8'>
    <title>MY PROJECT   </title>
</head>
<body>
<h1>The table of my project</h1>
{$this->createRow()}
{$this->editRow()}
{$this->deleteRow()}
{$this->recreateDb()}
{$this->getTable()}
</body>
</html>";
    }

    public function getTable()
    {
        $table = "";
        $table .= "
        <table border='1'>
  <tr>
    <th>id</th>
    <th>name</th>
    <th>status</th>
  </tr>
          ";
        foreach ($this->data as $row) {
            $color = $row['status'] == 1
                ? 'green'
                : 'red';

            $table .= "<tr style='background-color: {$color}'>";
            $table .= "<td>{$row['id']}";
            $table .= "<td>{$row['name']}";
            $table .= "<td>{$row['status']}</td>";
            $table .= "</tr>";
        }
        $table .= "</table>";
        return $table;
    }

    private function changeOperationsForm(
        string $color,
        bool $inputField,
        string $inputFieldName,
        string $placeholder,
        bool $additionalParam,
        bool $statusField,
        string $actionName,
        string $buttonName
    )
    {

        $additionalParam == true
            ? $additionalParam = "<input type='text' name='new_row_name' placeholder='Новое имя строки'>"
            : $additionalParam = '';
        $inputField == true
            ? $inputField = "<input type='text' name='{$inputFieldName}' placeholder='{$placeholder}'>"
            : $inputField = '';
        $statusField == true
            ? $statusField = "<input type='text' name='status' placeholder='* Статус строки'><br><p>* Введите статус строки. 0 (по умолчанию) - НЕ ВЫПОЛНЕН, 1 - ВЫПОЛНЕН</p>"
            : $statusField = '';
        return "
        <form method='post' style='background-color: {$color}'>
            {$inputField}
            {$additionalParam}
            {$statusField}
            <input type='hidden' name='action' value='{$actionName}'>
            <input type='submit' value='{$buttonName}'>
        </form>
        ";
    }

    public function createRow()
    {
        return $this->changeOperationsForm(
            'goldenrod',
            true,
            'row_name',
            'Имя новой строки',
            false,
            true,
            'create_row',
            'Создать новую строку'
        );
    }

    public function editRow()
    {
        return $this->changeOperationsForm(
            'aqua',
            true,
            'row_id',
            'Номер изменяемой строки',
            true,
            true,
            'edit_row',
            'Изменить строку'
        );
    }

    public function deleteRow()
    {
        return $this->changeOperationsForm(
            'darkviolet',
            true,
            'delete_row_id',
            'Номер удаляемой строки',
            false,
            false,
            'delete_row',
            'Удалить строку'
        );
    }

    public function recreateDb()
    {
        return $this->changeOperationsForm(
            'dimgrey',
            false,
            false,
            false,
            false,
            false,
            'recreate_table',
            'Пересоздать таблицу'
        );
    }

    public function errorPage(Exception $e)
    {
        die("
<html lang='ru'>
<head>
</head>
<body>
    <h1>Ошибка</h1>
    <p style='color: red'>{$e->getMessage()}</p>
</body>
</html>
");
    }

//    public function rowColor()
//    {
//        $_POST['status'] == 0
//        ? : $this->checkedIds[] = $_POST['id'];
//        $result = "";
//        foreach ($this->checkedIds as $id)
//        {
//            $result = "tr:nth-child($id) { backgroung-color: green }";
//        }
//        return $result;
//    }
}