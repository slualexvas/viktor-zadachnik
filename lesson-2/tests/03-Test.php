<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_03 extends BaseLesson2Test
{
    protected $taskIndex = '03';

    public function argsProvider()
    {
        return [
            ['aaa', "aaa aaa\n"],
            ['bbb', "bbb bbb\n"],
            ['1', "1 1\n"],
        ];
    }
}
