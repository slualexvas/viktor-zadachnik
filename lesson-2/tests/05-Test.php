<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_05 extends BaseLesson2Test
{
    protected $taskIndex = '05';

    public function argsProvider()
    {
        $usage = "Usage: {$this->getAddr()} [число]\n";

        return [
            [-2, "чётное\n"],
            [-1, "нечётное\n"],
            [0, "чётное\n"],
            [1, "нечётное\n"],
            [2, "чётное\n"],
            ['10 20', $usage],
            ['', $usage],
            ['aaa', $usage],
            [1.5, $usage],
        ];
    }
}
