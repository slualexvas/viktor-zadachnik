<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_04 extends BaseLesson2Test
{
    protected $taskIndex = '04';

    public function argsProvider()
    {
        return [
            [-2, "чётное\n"],
            [-1, "нечётное\n"],
            [0, "чётное\n"],
            [1, "нечётное\n"],
            [2, "чётное\n"],
            [100500, "чётное\n"],
            [100501, "нечётное\n"],
        ];
    }
}
