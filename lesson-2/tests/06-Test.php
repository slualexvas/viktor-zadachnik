<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_06 extends BaseLesson2Test
{
    protected $taskIndex = '06';

    public function argsProvider()
    {
        $usage = "Usage: {$this->getAddr()} [число от 0 до 100]\n";

        return [
            [1, "1 комментарий\n"],
            [11, "11 комментариев\n"],
            [21, "21 комментарий\n"],
            [2, "2 комментария\n"],
            [12, "12 комментариев\n"],
            [32, "32 комментария\n"],
            [3, "3 комментария\n"],
            [4, "4 комментария\n"],
            [5, "5 комментариев\n"],
            [92, "92 комментария\n"],
            [13, "13 комментариев\n"],
            [-1, $usage],
            [0, "0 комментариев\n"],
            [100, "100 комментариев\n"],
            [101, $usage],
            [1.5, $usage],
            ['aaa', $usage],
        ];
    }
}
