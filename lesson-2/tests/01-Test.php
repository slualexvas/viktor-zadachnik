<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_01 extends BaseLesson2Test
{
    protected $taskIndex = '01';

    public function argsProvider()
    {
        return [
            ['', "hello world\n"],
        ];
    }
}
