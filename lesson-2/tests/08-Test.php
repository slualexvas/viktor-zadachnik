<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_08 extends BaseLesson2Test
{
    protected $taskIndex = '08';

    public function argsProvider()
    {
        $usage = "Usage: {$this->getAddr()} [число] [знак: +-*/%] [число]\n";

        return [
            ['1 + 2', "3\n"],
            ['1 - 2', "-1\n"],
            ['1 "*" 2', "2\n"],
            ['1 / 2', "0.5\n"],
            ['1 % 2', "1\n"],
            ['1 + 2 + 3', $usage],
            ['a + b', $usage],
            ['1 x 2', $usage],
            ['1 / 0', "На 0 делить нельзя\n"],
            ['1 % 0', "На 0 делить нельзя\n"],
        ];
    }
}
