<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_07 extends BaseLesson2Test
{
    protected $taskIndex = '07';

    public function argsProvider()
    {
        $usage = "Usage: {$this->getAddr()} [целое число от 1 до 12]\n";

        return [
            ['0', $usage],
            ['1', "Зима\n"],
            ['2', "Зима\n"],
            ['3', "Весна\n"],
            ['4', "Весна\n"],
            ['5', "Весна\n"],
            ['6', "Лето\n"],
            ['7', "Лето\n"],
            ['8', "Лето\n"],
            ['9', "Осень\n"],
            ['10', "Осень\n"],
            ['11', "Осень\n"],
            ['12', "Зима\n"],
            ['13', $usage],
            ['1 2', $usage],
            ['', $usage],
        ];
    }
}
