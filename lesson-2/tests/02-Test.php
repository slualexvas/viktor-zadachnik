<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_02 extends BaseLesson2Test
{
    protected $taskIndex = '02';

    public function argsProvider()
    {
        return [
            ['', "hello world\n"],
        ];
    }
}
