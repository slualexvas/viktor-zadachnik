<?php
require_once __DIR__ . '/BaseLesson2Test.class.php';

class Test_09 extends BaseLesson2Test
{
    protected $taskIndex = '09';

    public function argsProvider()
    {
        $usage = "Usage: {$this->getAddr()} [целое число от 1 до 12]\n";

        return [
            ['0', $usage],
            ['1', "31\n"],
            ['2', "28\n"],
            ['3', "31\n"],
            ['4', "30\n"],
            ['5', "31\n"],
            ['6', "30\n"],
            ['7', "31\n"],
            ['8', "31\n"],
            ['9', "30\n"],
            ['10', "31\n"],
            ['11', "30\n"],
            ['12', "31\n"],
            ['13', $usage],
            ['1 2', $usage],
            ['', $usage],
        ];
    }
}
