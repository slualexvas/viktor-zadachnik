<?php
require __DIR__ . '/../../vendor/autoload.php';

class BaseLesson2Test extends PHPUnit\Framework\TestCase
{
    protected $taskIndex = '0';

    public function getAddr()
    {
        return __DIR__ . "/../answers/task-{$this->taskIndex}.php";
    }

    public function getCmd($arg)
    {
        return "php {$this->getAddr()} {$arg}";
    }

    /**
     * @dataProvider argsProvider
     */
    public function testBasic($arg, $etalonData)
    {
        if ($this->taskIndex == '0') {
            $this->assertTrue(true);
            return;
        }

        $testData = `{$this->getCmd($arg)}`;
        $this->assertEquals($etalonData, $testData);
    }

    public function argsProvider()
    {
        return [
            ['', null],
        ];
    }
}
