<?php

class Man
{
    private int $id = 1;
    private string $name = 'Вася';

    /**
     * @param int $id
     */
    public function setId(int $id): Man
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): Man
    {
        $this->name = $name;
        return $this;
    }
}

$man = new Man();
$man->setId(10)->setName('Миша');

var_dump($man);

/**
 * Задача: нужно переделать класс так, чтобы следующий кусок кода корректно отработал:
 *
 * $man->setId(10)->setName('Миша');
 * // Вот эта фигня называется "цепочка"
 */

