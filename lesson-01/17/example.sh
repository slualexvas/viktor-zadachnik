# 1) только те строки из grep-cases.txt, в которых есть буквосочетание "Кот"
grep 'Кот' grep-cases.txt

# 2) только те строки из grep-cases.txt, в которых есть буквосочетание "Кот" без различия размера букв (т.е "панакота" подходит)
grep -i 'Кот' grep-cases.txt

# 3) только те строки из grep-cases.txt, в которых есть буквосочетание "Кот" буквами одинакового размера (т.е. "кот" или "КОТ", но не "Кот").
grep -E "кот|КОТ" grep-cases.txt

# 4) только те строки из grep-cases.txt, в которых есть буквосочетание "Кот" и цифра сразу же после него.
grep -E 'Кот[0-9]' grep-cases.txt

# 5) только те строки из grep-cases.txt, в которых есть буквосочетание "Кот" и цифра после него (возможно, не сразу)
grep -E 'Кот.*[0-9]' grep-cases.txt

# 6) только те строки из grep-cases.txt, в которых есть буквосочетание "Кот" и ОТ ДВУХ ДО ТРЁХ ЦИФР ПОДРЯД сразу же после него.
grep -E 'Кот[0-9]{2,3}([^0-9]|$)' grep-cases.txt

# 7) Написать команду, которая ищет текст "ls" во всех файлах из этого урока
grep -r 'ls' ..